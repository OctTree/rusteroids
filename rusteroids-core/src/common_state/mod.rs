use crate::*; 

pub struct CommonState{
    pub gl:Rc<glow::Context>,
    pub window_width:usize, 
    pub window_height:usize, 
    pub shader_version:String,
}

impl CommonState{
    pub fn init_opengl(&self){
        unsafe{ 
            self.gl.viewport(0,0,self.window_width as i32 ,self.window_height as i32);
            self.gl.clear_color(0.5,0.5,0.5,1.0);
        }
    }   
}