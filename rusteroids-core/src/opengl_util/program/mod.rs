use glow::*; 
use std::rc::Rc;
use roxmltree;
use roxmltree::{NodeType,Document}; 
use std::collections::{HashMap,HashSet}; 

pub mod uniforms; 
pub mod attributes; 

use uniforms::Uniforms;
use attributes::Attributes; 

use crate::*; 


pub struct Program{
    id:Option<GlProgram>,
    shader_source:Option<String>,
    uniform_state:Uniforms,
    attr_state:Attributes,  
}

pub enum CompileError{
    XmlNoSource,
    XmlParseError,
    GlCompileError(String),
    GlLinkError(String),
}


impl Program{
    pub fn new(gl:&Context)->Program{
        let id = unsafe{ gl.create_program().expect("Cannot create program") };
        Program{
            id:Some(id),
            shader_source:None,
            uniform_state:Uniforms::new(),
            attr_state:Attributes::new(),
        }
    }
    
    pub fn with_shader_source(mut self,source:String)->Program{
        self.shader_source = Some(source);
        self
    }

    pub fn compile(self,gl:&Context)->Result<Program,CompileError>{
        self.bind(gl);
        match self.shader_source.clone() {
            Some(contents) =>{
                match Document::parse(&contents){
                    Ok( doc ) => {
                        self.ogl_build_program(doc,gl)
                    },
                    Err(_) => Err(CompileError::XmlParseError)
                }
            },
            None => Err(CompileError::XmlNoSource)
        }
    }

    pub fn bind(&self,gl:&Context){
        unsafe{
            gl.use_program(self.id); 
        }
    }

    pub fn get_id(&self) -> <Context as HasContext>::Program{
        self.id.expect("id not set. This means program obj not setup properly")
    }

    pub fn get_uniform_location(&self, uni_name: &str)->Option<GlUniformLocation>{
        self.uniform_state.get_uniform_location(uni_name)
    }

    fn ogl_build_program(mut self,doc:Document<'_>,gl:&Context)->Result<Program,CompileError>{
        let relevant_tags = [
            "header",
            "vert",
            "frag",
            "attributes",
            "uniforms"
        ];
        let mut relevant_tag_set = HashSet::new();
        let mut nodes = HashMap::new();
        let mut shaders = Vec::new(); 
        let mut header_source = String::new(); 

        for tag in relevant_tags.iter(){
            relevant_tag_set.insert(tag);
        }

        doc.root_element()
        .children()
        .filter(|n|n.is_element())
        .for_each(|n| {
            let tag = n.tag_name().name();
            if let Some(_) = relevant_tag_set.get(&tag){
                nodes.insert(tag, n);
            }
        });

        if let Some(n) = nodes.get("attributes"){
            self.attr_state
                .with_attribute_node(n);
        }

        if let Some(n) = nodes.get("uniforms"){ 
            self.uniform_state
                .with_uniform_node(n);
        }

        if let Some(node) = nodes.get("header") {
            let text_node = node.children()
                .next()
                .unwrap();
            header_source+=text_node.text().unwrap();
        }

        if let Some(node) = nodes.get("vert"){
            let text_node = node.children()
                .next()
                .unwrap();

            let vert_body = text_node.text()
                .unwrap(); 
  
            let vert_source = format!("{}\n{}\n{}\n{}\n{}",
                OPENGL_VER,
                header_source,
                self.attr_state.to_string(),
                self.uniform_state.to_string(),
                vert_body
            ); 

            // println!("vert source:\n{}",vert_source);
            if let Err(e) = Program::ogl_compile_shader(glow::VERTEX_SHADER,&mut shaders,gl,vert_source.as_ref()){
                return Err(e);
            }           
        }

        if let Some(node) = nodes.get("frag"){
            let text_node = node.children()
                .next()
                .unwrap();

            let frag_body = text_node.text().unwrap();

            let frag_source = format!("{}\n{}\n{}\n{}",
                OPENGL_VER,
                header_source,
                self.uniform_state.to_string(),
                frag_body
            ); 

            // println!("frag source:\n{}",frag_source);

            if let Err(e) = Program::ogl_compile_shader(glow::FRAGMENT_SHADER,&mut shaders,gl,frag_source.as_ref()){
                return Err(e);
            }   
        }
        
        for sid in &shaders{
            unsafe{
                gl.attach_shader(self.get_id(),sid.clone());
            }
        }

        unsafe{
            gl.link_program(self.get_id());
            if gl.get_program_link_status(self.get_id()) == false{
                return Err(CompileError::GlLinkError("linking failed".to_string()));
            }
        }
        self.uniform_state.find_uniform_ids(self.get_id(),gl);

        //delete shaders one the program is fully linked 
        for sid in shaders{
            unsafe{
                gl.delete_shader(sid);
            }
        }
        
        Ok(self)
    }
    fn ogl_compile_shader(shader_type:u32,shaders:&mut Vec< GlShader >,gl:&Context,source:&str)->Result<(),CompileError>{
        let shader_results = unsafe{ gl.create_shader(shader_type) };
        if let Ok(shader_id) = shader_results{
            unsafe{
                gl.shader_source(shader_id,source);
                gl.compile_shader(shader_id);
                if gl.get_shader_compile_status(shader_id) == false {
                    let err_msg = gl.get_shader_info_log(shader_id);
                    return Err(CompileError::GlCompileError(err_msg));
                }
            }
            shaders.push(shader_id);
        }
        Ok(())
    }    
}

#[allow(dead_code)]
fn print_type(nt:NodeType){
    match nt{
        NodeType::Root => println!("root"),
        NodeType::Element => println!("element"),
        NodeType::PI => println!("PI"),
        NodeType::Comment => println!("Comment"),
        NodeType::Text => println!("Text"),
    }
}
