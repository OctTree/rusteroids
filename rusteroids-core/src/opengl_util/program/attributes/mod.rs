use std::collections::*; 
use roxmltree;
use roxmltree::{Node}; 

struct AttribInfo{
    attr_loc:u32, 
    attr_type:String, 
}

impl AttribInfo{
    pub fn new(attr_loc:u32,attr_type:String)->AttribInfo{
        AttribInfo{
            attr_loc,
            attr_type,
        }
    }

    pub fn get_loc(&self)->u32{
        self.attr_loc
    }

    pub fn get_type(&self)->&String{
        &self.attr_type
    }
}

pub struct Attributes{
    names:Vec<String>,
    info_table:HashMap<String,AttribInfo>,
}

impl Attributes{
    pub fn new()->Attributes{
        Attributes{
            names:Vec::new(),
            info_table:HashMap::new(),
        }
    }

    pub fn with_attribute_node(&mut self, node:&Node){
        node.children()
        .filter(|n| n.is_element() )
        .for_each(|n| {
            let attrib_tuple = (
                n.attribute("location"),
                n.attribute("type"),
                n.attribute("name")
            ); 
            if let (Some(a_loc),Some(a_type),Some(a_name)) = attrib_tuple{
                if let Ok(loc) = a_loc.parse(){
                    let info = AttribInfo::new(loc,a_type.to_string());
                    let name = a_name.to_string(); 
                    self.info_table.insert(name.clone(),info);
                    self.names.push(name);
                }
            }
        });
    }
}

impl ToString for Attributes{
    fn to_string(&self)->String{
        let mut output = String::new();
        for name in &self.names{
            let info = self.info_table.get(name).unwrap();
            let line = format!("layout ( location = {} ) in {} {};\n",
                info.get_loc(),
                info.get_type(),
                name
            );
            output.push_str(line.as_str());
        }
        output
    }
}