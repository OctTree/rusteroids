use std::collections::HashMap; 
use roxmltree;
use roxmltree::{Node}; 
use glow::*; 

pub struct UniInfo{
    uname:String, 
    utype:String, 
}

use crate::{GlProgram,GlUniformLocation};


impl UniInfo{
    pub fn new(uname:String,utype:String)->UniInfo{
        UniInfo{
            uname,
            utype,
        }
    }

    pub fn get_name(&self)->&String{
        &self.uname
    }

    pub fn get_type(&self)->&String{
        &self.utype
    }
}

pub struct Uniforms{
    uni_info_lst:Vec<UniInfo>,
    loc_table:HashMap<String,GlUniformLocation >,
}

impl Uniforms{
    pub fn new()->Uniforms{
        Uniforms{
            uni_info_lst:Vec::new(),
            loc_table:HashMap::new(),
        }
    }

    pub fn with_uniform_node(&mut self,n:&Node){
        n.children()
        .filter(|n| n.is_element() )
        .for_each( |n| {
            let uniform_tuple = (
                n.attribute("type"),
                n.attribute("name")
            );
            if let (Some(utype),Some(uname)) = uniform_tuple{
                self.uni_info_lst.push( UniInfo::new(String::from(uname), String::from(utype)));
            }
        });
    }

    pub fn get_uniform_location(&self,uname:&str)->Option< GlUniformLocation> {
        let  query = self.loc_table.get(uname);
        match query{
            Some(location) => Some(location.clone()),
            None => None
        }
    }

    pub fn find_uniform_ids(&mut self,prog_id:GlProgram,gl:&Context){
        for UniInfo{uname , ..} in &self.uni_info_lst{
            let loc = unsafe{ gl.get_uniform_location(prog_id,uname.as_str())};
            self.loc_table.insert(uname.clone(),loc.unwrap());
        }
    }
}

impl ToString for Uniforms{ 
    fn to_string(&self) ->String{
        let mut result = String::new();
        for UniInfo{uname , utype} in &self.uni_info_lst{
            result += format!("uniform {} {};\n",&utype,&uname).as_str();
        }
        result
    }
}