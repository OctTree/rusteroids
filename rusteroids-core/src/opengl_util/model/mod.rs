use glow::*; 
use glow::{Context};
use std::slice::{from_raw_parts};
use std::mem::size_of;  
use crate::{GlVertexArray,GlBuffer};

pub struct VAO{
    id:GlVertexArray,
    vbo_list:Vec<VBO>, 
}

impl VAO{
    pub fn new(gl:&Context)->VAO{
        VAO{
            id: unsafe{
                gl.create_vertex_array().unwrap()
            },
            vbo_list: Vec::new(),
        }
    }

    pub fn bind(&self,gl:&Context){
        unsafe{
            gl.bind_vertex_array(Some(self.id));
        }
    }

    pub fn draw(&self, gl:&Context){
        unsafe{

        }
    }
}

pub struct VBO{
    id:Option<GlBuffer>,
    target:u32,
    usage:u32, 
    data:Vec<f32>,
    attrib_loc:u32, 
}

impl VBO{
    pub fn new(target:u32,usage:u32,attrib_loc:u32,gl:&Context)->VBO{
        let mut vbo = VBO{
            id:None,
            data: Vec::new(),
            target, 
            usage,
            attrib_loc,
        };
        
        unsafe{
            vbo.id = Some(gl.create_buffer().unwrap());
            gl.bind_buffer(vbo.target,vbo.id);
            gl.buffer_data_size(vbo.target,0,vbo.usage);
            gl.vertex_attrib_pointer_i32(attrib_loc, 4, glow::FLOAT, 0, 0);
            gl.enable_vertex_attrib_array(vbo.attrib_loc);
        }
        vbo 
    }
    
    pub fn with_data(mut self,data:Vec<f32>,gl:&Context)-> VBO{
        self.data = data; 
        unsafe{
            gl.bind_buffer(self.target,self.id);
            gl.buffer_data_size(self.target,self.data.len() as i32,self.usage);
        }
        self
    }

    pub fn update(&self,gl:&Context){
        let buffer_ptr =  self.data.as_ptr() as *const u8;
        let num_bytes  =  self.data.len()*size_of::<f32>();
        let slice:&[u8]=  unsafe{from_raw_parts( buffer_ptr,num_bytes)};
        unsafe{
            gl.bind_buffer(self.target,self.id);
            gl.buffer_sub_data_u8_slice(self.target,0,slice);
        }
    }
}


