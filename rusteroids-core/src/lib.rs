use glow::*;
// use std::sync::{Arc};
use std::rc::{Rc};


#[cfg(all(target_arch = "wasm32"))]
pub static OPENGL_VER:&'static str = "#version 300 es";

#[cfg(any(target_arch = "x86",target_arch="x86_64"))]
pub static OPENGL_VER:&'static str = "#version 330";


pub type GlProgram = <Context as HasContext>::Program; 
pub type GlUniformLocation = <Context as HasContext>::UniformLocation; 
pub type GlShader = <Context as HasContext>::Shader; 
pub type GlVertexArray = <Context as HasContext>::VertexArray; 
pub type GlBuffer = <Context as HasContext>::Buffer; 

pub mod common_state; 
pub mod opengl_util; 
pub mod io_util; 

#[cfg(test)]
pub mod tests;

pub trait Model{
    fn init(&mut self); 
    fn draw(&self,gl:&Context);
}

pub trait ProgramState<OutputState>{
    fn init_context(width:usize,height:usize,title:Option<&'static str>)->OutputState;
    fn run_main_loop(self);
}