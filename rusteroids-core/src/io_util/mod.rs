#[cfg(all(target_arch = "wasm32", feature = "web-sys"))]
mod io_wasm;
#[cfg(all(target_arch = "wasm32", feature = "web-sys"))]
pub use io_wasm::*; 

#[cfg(any(target_arch = "x86",target_arch="x86_64"))]
mod io_desktop; 
#[cfg(any(target_arch = "x86",target_arch="x86_64"))]
pub use io_desktop::*; 

use std::io::{BufReader,BufWriter};
/*
Acts as a layer for both wasm32 and desktop targets
*/
pub trait IO{
    fn new()->Self; 
    fn open(&self,path:&str,mode:u32);
    fn read_text(&self)->Option<String>; 
    fn close(&self); 
}