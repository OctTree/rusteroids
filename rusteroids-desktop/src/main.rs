
pub mod glutin_state;
use rusteroids_core::*;
use glutin_state::*; 

fn main() {
    let state = GlutinState::init_context(640,480,Some("rusteroids"));
    state.run_main_loop();
}
