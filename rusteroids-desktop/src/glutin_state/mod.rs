use crate::*; 

use rusteroids_core::{
    common_state::CommonState,
    opengl_util::program::Program
}; 

use std::rc::{Rc};
use std::slice::*;
use glow::HasContext;


pub struct GlutinState{
    pub common:CommonState,
    event_loop:glutin::event_loop::EventLoop<()>,
    windowed_context:glutin::ContextWrapper<glutin::PossiblyCurrent,glutin::window::Window>,
}

impl ProgramState<GlutinState> for GlutinState{
    fn init_context(width:usize,height:usize,title_opt:Option<&'static str>)->GlutinState{
        let (gl, event_loop, windowed_context,shader_version) = {
            let el = glutin::event_loop::EventLoop::new();
            let wb = glutin::window::WindowBuilder::new()
                .with_inner_size(glutin::dpi::LogicalSize::new(width as f32,height as f32))
                .with_title(title_opt.unwrap_or("glutin window"));

            let windowed_context = glutin::ContextBuilder::new()
                .with_vsync(true)
                .build_windowed(wb, &el)
                .unwrap();

            let windowed_context = unsafe{windowed_context.make_current().unwrap()};

            let context = glow::Context::from_loader_function(|s| {
                windowed_context.get_proc_address(s) as *const _
            });

            (context, el, windowed_context, String::from("#version 330") )
        };

        GlutinState{
            common:CommonState{
                gl:Rc::new(gl),
                window_height:height,
                window_width:width, 
                shader_version
            },
            event_loop,
            windowed_context,
        }
    }
    fn run_main_loop(self){
        use glutin::event::{Event, WindowEvent};
        use glutin::event_loop::ControlFlow;

        let windowed_context = self.windowed_context; 
        let event_loop = self.event_loop;

        let state = self.common;
        let gl = state.gl.clone();

        state.init_opengl();


        let mut t = 0.0; 
        let verts = [
            1.0f32,0.0,0.0,1.0,
             0.0,1.0,0.0,1.0,
            -1.0,0.0,0.0,1.0,
        ];
        let slice:&[u8] = unsafe{ from_raw_parts(verts.as_ptr() as *const u8,verts.len()*4 )  } ;

        let source = std::fs::read_to_string("./resources/glsl_programs/test_program.xglsl").unwrap();
        
        let program = Program::new(gl.as_ref())
            .with_shader_source(source)
            .compile(gl.as_ref())
            .ok()
            .unwrap();

        let (_vao,_vbo)  = unsafe{
            let vao = gl.create_vertex_array().unwrap();
            gl.bind_vertex_array(Some(vao));
            let vbo = gl.create_buffer().unwrap();
            gl.bind_buffer(glow::ARRAY_BUFFER,Some(vbo));
            gl.buffer_data_u8_slice(glow::ARRAY_BUFFER, slice, glow::STATIC_DRAW);
            gl.vertex_attrib_pointer_f32(1, 4,glow::FLOAT,false,0,0);
            gl.enable_vertex_attrib_array(1);
            (vao,vbo)
        };
   

        let time_loc = program.get_uniform_location("time").unwrap();

        event_loop.run(move |event, _, control_flow| {
            *control_flow = ControlFlow::Wait;
            let _state = &state;
            let t = &mut t; 
            let gl = gl.as_ref(); 

            match event {
                Event::LoopDestroyed => {
                    return;
                }

                Event::MainEventsCleared => {
                    windowed_context.window().request_redraw();
                }

                Event::RedrawRequested(_) => {
                    unsafe{
                        *t+=0.01;
                    

                        program.bind(gl);
                        gl.clear(glow::COLOR_BUFFER_BIT);
                        gl.uniform_1_f32(Some(time_loc),*t);
                      
                        gl.draw_arrays(glow::TRIANGLES, 0, 3);
                        windowed_context.swap_buffers().unwrap();
                    }
                }

                Event::WindowEvent { ref event, .. } => match event {
                    WindowEvent::Resized(physical_size) => {
                        let (w,h) = (physical_size.width as i32,physical_size.height as i32);
                        unsafe{
                            gl.viewport(0,0,w,h)
                        };
                        
                        windowed_context.resize(*physical_size);
                    }
                    WindowEvent::CloseRequested => {
                        *control_flow = ControlFlow::Exit
                    }
                    _ => (),
                },
                _ => (),
            }
        });
    }
}