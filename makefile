wasm32: 
	cd ./rusteroids-wasm32 ; wasm-pack build --target=web && sudo cp -rf ./pkg/ /var/www/html/ ; cd .. ; 

desktop: 
	cargo build -p rusteroids-desktop --release 

.PHONY: wasm32 desktop 