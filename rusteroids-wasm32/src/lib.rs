use glow::*;
use wasm_bindgen::prelude::*;
use rusteroids_core;
use rusteroids_core::*; 
use wasm_bindgen::JsCast;
use web_sys; 
use wasm_bindgen_futures::{JsFuture,future_to_promise};
use js_sys::{Promise,Function,Uint8Array};
use js_sys;
use web_sys::{console,FileReader};

macro_rules! console_log {
    ($($t:tt)*) => (console::log_1(&format_args!($($t)*).to_string().into()))
}

pub static BINARY_BLOB:Option<Vec<u8>> = None; 

use websys_state::WebsysState;

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;




#[wasm_bindgen]
extern {
    fn alert(s: &str);
}

#[wasm_bindgen]
pub fn greet() {
    alert("This is rust code right now brush");
}

#[wasm_bindgen]
pub fn add(a:f32,b:f32)->f32 {
    a*a+b*b
}

#[wasm_bindgen]
pub fn print_result(file_reader: FileReader) -> String {
    match file_reader.result() {
        Ok(s) => s.as_string().unwrap(),
        Err(e) => String::new()
    }
}

#[wasm_bindgen(module = "/js/js_io_util.js")]
extern "C" {
    pub type JsIoUtil;
    #[wasm_bindgen(constructor)]
    pub fn new() -> JsIoUtil;
    #[wasm_bindgen(method)]
    pub fn requestFile(this:&JsIoUtil,url:String,listenerIndex:u32,callback:Option<&Function>);
    #[wasm_bindgen(method)]
    pub fn readFileAsync(this:&JsIoUtil,url:String)->Promise;
}

// lifted from the `console_log` example
#[wasm_bindgen]
extern "C" {
    #[wasm_bindgen(js_namespace = console)]
    fn log(s: &str);
}






#[wasm_bindgen(start)]
pub fn main(){


    let _p = future_to_promise( async{

        
        // let js_file_promise = JsIoUtil::new()
        //     .readFileAsync(String::from("poem.txt"));
            
        // let js_buffer = JsFuture::from(js_file_promise)
        //     .await?
        //     .dyn_into::<Uint8Array>()
        //     .unwrap();

        // let byte_vec = js_buffer.to_vec();
        // let s = String::from_utf8(byte_vec).unwrap();
        // console_log!("I did it bros: {}",s);

        let state = WebsysState::init_context(640,480,Some("rusteroids"));
        state.run_main_loop();

        Ok(JsValue::UNDEFINED)
    });
}

pub mod websys_state;