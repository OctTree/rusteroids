
use rusteroids_core::{
    common_state::CommonState,
};

use crate::ProgramState;

use wasm_bindgen::JsCast;
use web_sys; 
use glow::*; 
use wasm_bindgen::prelude::*;
use wasm_bindgen_futures::{JsFuture,future_to_promise};

use std::rc::Rc;
use std::slice;
use web_sys::{console,Request,RequestInit,RequestMode,Response,XmlHttpRequest,XmlHttpRequestResponseType,XmlHttpRequestEventTarget,EventTarget};
use js_sys::{Function,Promise,Uint8Array};
use futures;
use futures::executor::{block_on};
use std::collections::{HashMap};
use std::cell::RefCell;
use std::thread_local; 
use std::thread::LocalKey; 
use rusteroids_core::opengl_util::program::Program; 

struct Image{
    raw_data:Vec<u8>, 
}

impl Image{
    pub fn new()->Image{
        Image{
            raw_data:Vec::new()
        }
    }
    pub fn to_heap(self)->Box<Image>{
        Box::new(
            self
        )
    }
}

impl LoadEventListener for Image{
    fn process_binary(&mut self, binary_data:Vec<u8>){
        console_log!("Image event listener fired! YAY!");
    }
}

pub trait LoadEventListener{
    fn process_binary(&mut self,binary_data:Vec<u8>);
    fn add(&mut self){

    }
}
pub static mut LOADED_FILE_LOOKUP_TABLE:Option<HashMap<String,Vec<u8>>> = None;
pub static mut LOAD_CB:Option<Closure<(dyn FnMut(JsValue,JsValue,JsValue,JsValue) + 'static)>> = None; 
pub static mut IO_EVENT_STACK:Option<Vec<Rc<RefCell<Box<dyn LoadEventListener>>>>> = None; 




fn make_get_request(obj:Rc<RefCell<Box<dyn LoadEventListener>>>,path:String){
    unsafe{
        let stack = IO_EVENT_STACK.as_mut().unwrap();
        stack.push(obj);
        let index = (stack.len()-1) as u32;

        super::JsIoUtil::new()
            .requestFile(
                path,
                index,
                Some(
                    LOAD_CB
                        .as_mut()
                        .unwrap()
                        .as_ref()
                        .unchecked_ref()
                )
            );
    }
}

pub struct WebsysState{
    common:Option<CommonState>,
    #[cfg(all(target_arch = "wasm32", feature = "web-sys"))]
    render_loop:Option<glow::RenderLoop>,
}

impl ProgramState<WebsysState> for WebsysState{
    fn init_context(width:usize,height:usize,title:Option<&'static str>)->WebsysState{
        
        #[cfg(all(target_arch = "wasm32", feature = "web-sys"))]
        let (_window, gl, _events_loop, render_loop, shader_version) = {
            let canvas = web_sys::window()
                .unwrap()
                .document()
                .unwrap()
                .get_element_by_id("canvas")
                .unwrap()
                .dyn_into::<web_sys::HtmlCanvasElement>()
                .unwrap();
            let webgl2_context = canvas
                .get_context("webgl2")
                .unwrap()
                .unwrap()
                .dyn_into::<web_sys::WebGl2RenderingContext>()
                .unwrap();
            (
                (),
                glow::Context::from_webgl2_context(webgl2_context),
                (),
                glow::RenderLoop::from_request_animation_frame(),
                "#version 300 es",
            )
        };

        #[cfg(all(target_arch = "wasm32", feature = "web-sys"))]{
            WebsysState{
                common:Some(CommonState{
                    gl:Rc::new(gl),
                    window_width:640,
                    window_height:480,
                    shader_version:String::from(shader_version),
                }),
                render_loop:Some(render_loop),
            }
        }

        #[cfg(not(all(target_arch = "wasm32", feature = "web-sys")))]{
            WebsysState{
                common:None,
            }
        }
    }

    fn run_main_loop(self){
        #[cfg(all(target_arch="wasm32", feature = "web-sys"))]{
            let _p = future_to_promise(async {

                console_log!("future is running....");

                let render_loop = self.render_loop.unwrap(); 
                let mut state = self.common.unwrap(); 
                let gl = state.gl.clone();

                state.init_opengl();

                let mut t = 0.0; 
                let verts = [
                    1.0f32,0.0,0.0,1.0,
                    0.0,1.0,0.0,1.0,
                    -1.0,0.0,0.0,1.0,
                ];
                let slice:&[u8] = unsafe{ slice::from_raw_parts(verts.as_ptr() as *const u8,verts.len()*4 )  } ;

            
                //load source code START
                let js_file_promise = super::JsIoUtil::new()
                    .readFileAsync(String::from("test_program.xglsl"));

                let js_buffer = JsFuture::from(js_file_promise)
                    .await?
                    .dyn_into::<Uint8Array>()
                    .unwrap();

                let byte_vec = js_buffer.to_vec();
                let program_source = String::from_utf8(byte_vec).unwrap();
                console_log!("source:\n{}\n",program_source);

                use rusteroids_core::opengl_util::program::*; 
                let compile_result = Program::new(gl.as_ref())
                    .with_shader_source(program_source)
                    .compile(gl.as_ref());

                let program_opt = match compile_result{
                    Ok(prog) => Some(prog),
                    Err(err)=> match err{
                        CompileError::GlLinkError(msg) => {
                            console_log!("Link Error!");
                            None
                        },
                        CompileError::GlCompileError(msg) => {
                            console_log!("shader error \n {}", msg);
                            None
                        },
                        _ =>None,
                    }
                };

                let program = program_opt.unwrap();
      
                
                let (_vao,_vbo) =unsafe{
                    program.bind(gl.as_ref());

                    let vao = gl.create_vertex_array().unwrap();
                    gl.bind_vertex_array(Some(vao));
                    let vbo = gl.create_buffer().unwrap();
                    gl.bind_buffer(glow::ARRAY_BUFFER,Some(vbo));
                    gl.buffer_data_u8_slice(glow::ARRAY_BUFFER, slice, glow::STATIC_DRAW);
                    gl.vertex_attrib_pointer_f32(1, 4,glow::FLOAT,false,0,0);
                    gl.enable_vertex_attrib_array(1);
                    
                    (vao,vbo)
                };

                let time_loc = program.get_uniform_location("time").unwrap();

                render_loop.run(move |running: &mut bool| {
                    let state = &mut state; 
                    let t = &mut t; 
                    let time_loc = time_loc.clone(); 
                    let gl = gl.as_ref(); 
                    unsafe{
                        *t+=0.01; 
                        gl.clear(glow::COLOR_BUFFER_BIT);
                        program.bind(gl);
                        gl.draw_arrays(glow::TRIANGLES, 0, 3);
                        gl.uniform_1_f32(Some(time_loc),*t);  
                    }
                });

                Ok(JsValue::UNDEFINED)
            });   
        }
    }
}




// fn init_context(width:usize,height:usize,title:Option<&'static str>)->WebsysState{
//     unsafe{
//         IO_EVENT_STACK = Some(Vec::new());
//         LOADED_FILE_LOOKUP_TABLE = Some(HashMap::new());
//         LOAD_CB = Some(
//             Closure::wrap(Box::new(move |js_array_obj:JsValue,js_trait_index:JsValue,js_url:JsValue,js_status:JsValue|{
//                 let js_byte_buffer:Uint8Array = js_array_obj.dyn_into().unwrap();
//                 //get byte array from JS 
//                 let byte_vec = js_byte_buffer.to_vec();
//                 //get trait index from JS
//                 let trait_index = js_trait_index.as_f64().unwrap() as usize; 
//                 //get filename from JS 
//                 let url = js_url.as_string().unwrap();
//                 //get http status 
//                 let status = js_status.as_f64().unwrap() as usize;


//                 console_log!("trait index = {}, url = {} status = {}", trait_index, url, status);
                
//                 let text = String::from_utf8(byte_vec.clone()).unwrap_or_default();
//                 LOADED_FILE_LOOKUP_TABLE
//                     .as_mut()
//                     .unwrap()
//                     .insert(url,byte_vec.clone());
//                 console_log!("callback fired! heres the text = {}",text);

//                 //execute event listener for object 
//                 IO_EVENT_STACK
//                     .as_mut()
//                     .unwrap()[trait_index]
//                     .borrow_mut()
//                     .process_binary(byte_vec);
                
//             }
//         ) as Box<dyn FnMut(JsValue,JsValue,JsValue,JsValue)>));
//     };
//     // let to:Box<dyn LoadEventListener> = Box::new(Image::new());
//     // let image = Rc::new(RefCell::new(to));
//     // make_get_request(image.clone(),"poem.txt".to_string());

//     // let fut = JsFuture::from( super::JsIoUtil::new().readFileAsync("poem.txt".to_string()));
