export function name() {
    return 'Rust';
}

export class JsIoUtil{
    constructor() {
        /*
        nothing for now
        */
    }
    requestFile(url,listenerIndex,callback){
        console.log("called http get");
        var oReq = new XMLHttpRequest();
        oReq.open("GET", url , true);
        oReq.responseType = "arraybuffer";
        oReq.onload = function (oEvent) {
            var arrayBuffer = oReq.response; // Note: not oReq.responseText
            var status = oReq.status;
            if (arrayBuffer) {
                var byteArray = new Uint8Array(arrayBuffer);
                callback(byteArray,listenerIndex,url,status);
            }
        };
        oReq.send(null);
    }

    readFileAsync(url){
        return new Promise( (resolve,reject) =>{
            var xml = new XMLHttpRequest();
            xml.open("GET",url,true);
            xml.responseType = "arraybuffer";
            xml.onload = (event) => {
                var arrayBuffer = xml.response; 
                return (arrayBuffer) ? resolve( new Uint8Array(arrayBuffer) ) : reject("read failed");
            };
            xml.send(null);
        });
    }
    
}